#ifndef __MEMNODE_H
#define __MEMNODE_H
#include "memory_structures.h"

#ifdef __cplusplus
extern "C" {
#endif

Mnode *create_node(unsigned int size, void *head, bool allocated, Mnode *pNext, Mnode *pPrev);

void remove_node(Mnode *pnode);

Mnode *find_free_block(MemContext *context, unsigned int size_requiered);

Mnode *divide_node(Mnode *node, unsigned int size);

Mnode *get_allocated_node(MemContext *context, void *address);

Mnode *combine_with_next_block(MemContext *context, Mnode *const pnode);

Mnode *combine_with_prev_block(MemContext *context, Mnode *const pnode);

Mnode *combine_adjacent_free_blocks(MemContext *context, Mnode *pnode);

void show_node(Mnode *node);
#ifdef __cplusplus
}
#endif

#endif
