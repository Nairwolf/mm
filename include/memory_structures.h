#ifndef __MEMORY_STRUCTURES_H
#define __MEMORY_STRUCTURES_H

#define TRUE 1
#define FALSE 0
typedef char bool;

// a Mnode stores meta-data, and refers to a memory block
typedef struct mnode {
  unsigned int size;
  void *head; //pointer to a memory block
  bool allocated; // 1 if it's allocated, 0 else
  struct mnode *pNext;
  struct mnode *pPrev;
} Mnode;

typedef struct context {
  Mnode *head_node;  // init to 0
  Mnode *tail_node;  // init to 0
  void *(*platform_malloc)(unsigned long size);  // pointer to platform specific malloc
  void (*platform_free)(void *p);          // pointer to platform specific free
} MemContext;

#endif
