#ifndef __MEMMNG_H
#define __MEMMNG_H
#include "memory_structures.h"

#ifdef __cplusplus
extern "C" {
#endif

int fmalloc_init (MemContext *context, void *heap, unsigned int heap_size);

int fmalloc_exit (MemContext *context);

void *fmalloc(MemContext *context, unsigned int size);

void ffree(MemContext *context, void *address);

void display_nodes(MemContext *context);

//Not implemented yet
//void *fcalloc(int num, int size);
//void *frealloc(void *address, int newsize);

#ifdef __cplusplus
}
#endif

#endif


