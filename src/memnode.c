#include <stdio.h>
#include <stdlib.h>
#include "memnode.h"
#include "memory_structures.h"

Mnode *create_node(unsigned int size, void *head, bool allocated, Mnode *pNext, Mnode *pPrev)
{
  //Use Dynamic Memory Allocation actually, can be changed later.
  Mnode *new_node = malloc(sizeof(Mnode)); 
  
  if (new_node != NULL)
  {
    new_node->size = size;
    new_node->head = head;
    new_node->allocated = allocated;
    new_node->pNext = pNext;
    new_node->pPrev = pPrev;
  }
  
  return new_node;
}

void remove_node(Mnode *pnode)
{
  //Use Dynamic Memory Allocation actually, can be changed later.
  free(pnode);
}

Mnode *find_free_block(MemContext *context, unsigned int size_requiered)
{
  Mnode *tmp_node = context->head_node;
  Mnode *optimum_node = NULL; 

  int min_surplus = -1;

  do{

    if (tmp_node->allocated == FALSE)
    {

      int tmp_surplus = tmp_node->size - size_requiered;
      
      if (min_surplus < 0) //Look for the first free block available to define min_surplus
      {

        if (tmp_surplus >= 0)
        {
          min_surplus = tmp_surplus;
          optimum_node = tmp_node;
        }
      
      }
      
      if (tmp_surplus >= 0 && tmp_surplus < min_surplus)
      {
        min_surplus = tmp_surplus;
        optimum_node = tmp_node;
      }
    }
    
    tmp_node = tmp_node->pNext;

  } while(tmp_node != NULL);
  
  return optimum_node;
}

Mnode *divide_node(Mnode *node, unsigned int size)
{  
  //Create new Mnode
  unsigned int new_size = node->size - size;
  void *new_head = node->head + size; //DH : Need to manage correctly address operations
  bool allocated = node->allocated;
  Mnode *new_next_node = node->pNext;
  Mnode *new_prev_node = node;
  Mnode *residual_node = create_node(new_size, new_head, allocated, new_next_node, new_prev_node);
 
  //Modify actual node
  node->size = size;
  node->pNext = residual_node;

  //Modify linkage after residual_node
  if (residual_node->pNext != NULL)
    residual_node->pNext->pPrev = residual_node;

  return node;
}

Mnode *get_allocated_node(MemContext *context, void *address)
{
  Mnode *tmp_node = context->head_node;
  bool pBlock_is_in_List = FALSE;
  
  while( tmp_node != context->tail_node)
  {
    if (address == tmp_node->head && tmp_node->allocated == TRUE)
    {
      pBlock_is_in_List = TRUE;
      break;
    }
    else
      tmp_node = tmp_node->pNext;
  }
  
  if (pBlock_is_in_List == TRUE)
    return tmp_node;
  else
    return NULL;
}

Mnode *combine_with_next_block(MemContext *context, Mnode *const pnode)
{  
  if (pnode->pNext != NULL)
  {
    Mnode *const pnode_next = pnode->pNext;
    
    //Create new Mnode
    unsigned int new_size = pnode->size + pnode_next->size;
    void *new_head = pnode->head;
    bool allocated = pnode->allocated;
    Mnode *new_next_node = pnode_next->pNext; 
    Mnode *new_prev_node = pnode->pPrev; 
    Mnode *new_node = create_node(new_size, new_head, allocated, new_next_node, new_prev_node);
    
    //Update linkage - Left Side
    if (pnode->pPrev != NULL)
      pnode->pPrev->pNext = new_node;
    else
      context->head_node = new_node;
    
    //Update linkage - Right Side
    if (pnode_next->pNext != NULL)
      pnode_next->pNext->pPrev = new_node;
    else
      context->tail_node = new_node;
    
    //Remove pnode and pnode_next
    remove_node(pnode);
    remove_node(pnode_next);
    
    return new_node;
  }
  else
    return pnode;
}

Mnode *combine_with_prev_block(MemContext *context, Mnode *const pnode)
{
  if (pnode->pPrev != NULL)
  {
    Mnode *const pnode_prev = pnode->pPrev; 
    return combine_with_next_block(context, pnode_prev);
  }
  else
    return pnode;
}

Mnode *combine_adjacent_free_blocks(MemContext *context, Mnode *pnode)
{
  if (pnode->pNext != NULL)
  {
    if (pnode->pNext->allocated == FALSE)
      pnode = combine_with_next_block(context, pnode);
  }
  
  if (pnode->pPrev != NULL)
  {
    if (pnode->pPrev->allocated == FALSE)
      pnode = combine_with_prev_block(context, pnode);
  }
  
  return pnode;
}

void show_node(Mnode *node)
{
  if (node !=NULL)
  {
    printf("address of node : %p\n", node);
    printf("size            : %u\n", node->size);
    printf("head            : %p\n", node->head);
  
    if (node->allocated == TRUE)
      printf("allocated       : YES\n");
    else
      printf("allocated       : NO\n");

    if (node->pNext != NULL)
      printf("pNext->head     : %p\n", node->pNext->head);
    else
      printf("pNext is NULL\n");

    if (node->pPrev != NULL)
      printf("pPrev->head     : %p\n", node->pPrev->head);
    else
      printf("pPrev is NULL\n");
  }
  else
    printf("node is NULL\n");
 }
