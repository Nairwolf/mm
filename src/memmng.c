#include <stdlib.h>
#include <stdio.h>
#include "memmng.h"
#include "memory_structures.h"
#include "memnode.h"

int fmalloc_init(MemContext *context, void *heap, unsigned int heap_size)
{
  Mnode *pNext = NULL;
  Mnode *pPrev = NULL;
  Mnode *node = create_node(heap_size, heap, FALSE, pNext, pPrev);
  if (node != NULL)
  {
    context->head_node = node;
    context->tail_node = node;
    return 1;
  }
  else
    return 0;
}

int fmalloc_exit (MemContext *context)
{
  Mnode *node = context->head_node;
  
  while (node != context->tail_node)
  {
    Mnode *tmp_node = node;
    node = node->pNext;
    context->platform_free(tmp_node);
  }
  context->platform_free(node);
  return 1;
}

void *fmalloc(MemContext *context, unsigned int size)
{
  Mnode *pBlock = find_free_block(context, size);
  
  if (pBlock == NULL)
  {
    printf("no free block available of size : %u\n", size);
    return NULL;
  }
  
  pBlock = divide_node(pBlock, size);
  pBlock->allocated = TRUE;
  
  if (context->tail_node == pBlock)
    context->tail_node = pBlock->pNext;

  return pBlock->head;
  //return (void *)pBlock->head;
}

void ffree(MemContext *context, void *address)
{
  Mnode *pnode = get_allocated_node(context, address);
  if (pnode != NULL)
  {
    //Free the memory block
    Mnode *new_free_node = combine_adjacent_free_blocks(context, pnode);
    new_free_node->allocated = FALSE;
  }
  else
  {
    //There is an error. The address can be already free, or it's not a valid address
    //DH : Find how to create an error message for the user.
    printf("error : %p doesn't match with an allocated memory block.\n"
           "Try again.\n", address);
  }
}

void display_nodes(MemContext *context)
{
  int number_nodes = 0;
  Mnode *tmp_node = context->head_node;

  printf("---------- general information ------------\n");
  printf("**** head node ****\n");
  show_node(context->head_node);

  printf("**** tail node ****\n");
  show_node(context->tail_node);

  printf("----------- display all nodes -------------\n");
  while (tmp_node != NULL)
  {
    printf("**** node %d ****\n", number_nodes);
    show_node(tmp_node);
    tmp_node = tmp_node->pNext;
    number_nodes++;
  }

  printf("-------------- end of display -------------\n");  
}

//Not implemented
//void *fcalloc(int num, int size)
//{
//  return NULL;
//}

//void *frealloc(void *address, int newsize)
//{
//  return NULL;
//}
