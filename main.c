#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "memmng.h"
#include "memory_structures.h"

int main(){

unsigned int heap_size = 100*sizeof(uint8_t);
uint8_t *memory = malloc(heap_size);

//Create the context of the memory manager.
MemContext *mycontext = malloc(sizeof(MemContext));

//The user specifies the malloc and free methods he wants to be used
mycontext->platform_malloc = malloc;
mycontext->platform_free = free;

//The user initializes the memory manager
int success = fmalloc_init(mycontext, memory, heap_size);

if (success != 0)
{
  //The user has 100bytes which can be used. 
  uint8_t *data1 = fmalloc(mycontext, sizeof(uint8_t));
  uint16_t *data2 = fmalloc(mycontext, 2*sizeof(uint8_t));

  //Do what you want with data1 and data2
  *data1 = 255;
  *data2 = 256;

  printf("*** value of data allocated ***\n");
  printf("data1 = %u\n", *data1);
  printf("data2 = %u\n", *data2);

  printf("*** size of data allocated ***\n");
  printf("sizeof(data1) = %lu\n", sizeof(*data1));
  printf("sizeof(data2) = %lu\n", sizeof(*data2));

  //Free these datas
  ffree(mycontext, data1);
  ffree(mycontext, data2);
  
  return EXIT_SUCCESS;
}
else
  return EXIT_FAILURE;
}
